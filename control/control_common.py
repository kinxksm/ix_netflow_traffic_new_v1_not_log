# -*- coding: utf-8 -*-
'''
Created on 2014. 11. 17.

@author: ko
'''

from datetime import datetime, timedelta
import re

from common.commonDb import CommonDb
from db.tCompanyTrafficHour import TCompanyTrafficHourDao
from db.tCompanyTrafficIpInfo import TCompanyTrafficIpInfoDao 


class ControlCommonFunc(object):
    #
    def __init__(self, commonFuncObj, loggerObj, confObj):
        
        session_name = "nfdump_session"
        
        self.className = self.__class__.__name__
        self.commonFuncObj = commonFuncObj 
        self.loggerObj = loggerObj
        self.commonDbObj = CommonDb(confObj, loggerObj)
        
        
        self.nfdump_src_aggre_cmd_fmt = self.commonFuncObj.getConfValue(confObj, session_name, "nfdump_src_aggre_cmd_fmt", "nfdump -r {nfcapd_data_file_path} -A srcip '(src ip {ip})' | grep \"total bytes\" | sed s/\", total packets.*\"//g | sed s/\"^.*bytes: \"//g &&")
        self.nfdump_dst_aggre_cmd_fmt = self.commonFuncObj.getConfValue(confObj, session_name, "nfdump_dst_aggre_cmd_fmt", "nfdump -r {nfcapd_data_file_path} -A dstip '(dst ip {ip})' | grep \"total bytes\" | sed s/\", total packets.*\"//g | sed s/\"^.*bytes: \"//g &&")
        self.nfdump_get_timestamp_cmd_fmt = self.commonFuncObj.getConfValue(confObj, session_name, "nfdump_get_timestamp_cmd_fmt", "nfdump -r {nfcapd_data_file_path} -A srcip '(dst ip {ip})' | grep \"Time window\"")
        
        self.src_use_ip_confirm_cmd_fmt = self.commonFuncObj.getConfValue(confObj, session_name, "src_use_ip_confirm_cmd_fmt", "nfdump -R {one_hour_netflow_file_path} -A srcip 'net {cidr}' -o \"fmt:%sa\"")
        self.dst_use_ip_confirm_cmd_fmt = self.commonFuncObj.getConfValue(confObj, session_name, "dst_use_ip_confirm_cmd_fmt", "nfdump -R {one_hour_netflow_file_path} -A dstip 'net {cidr}' -o \"fmt:%da\"")
        
        self.sampling_rate = self.commonFuncObj.getConfValue(confObj, session_name, "sampling_rate", 10000)
        self.interval_minutes = int(self.commonFuncObj.getConfValue(confObj, session_name, "interval_minutes", 300))
    
    #
    def mainProcess(self, nfcapd_data_path, start_time):
        
        connectDbObj = None
        
        try:
            connectDbObj = self.commonDbObj.connectDB()
            
            # db로부터 수집하고자 하는 ip 트래픽 정보의 대역을 취득
            company_traffic_ip_info_list = self.getCompanyTrafficIpInfoList(connectDbObj)
            
            tCompanyTrafficHourDaoObj = TCompanyTrafficHourDao(connectDbObj, self.loggerObj)

            execute_time_str = (start_time + timedelta(hours=1)).strftime('%Y%m%d%H%M')
            
            one_hour_netflow_file_path = "%s/nfcapd.%s:nfcapd.%s" % (nfcapd_data_path, start_time.strftime('%Y%m%d%H%M'),
                                                              (start_time + timedelta(minutes=55)).strftime('%Y%m%d%H%M'))
            
            componentDictObj = self.getComponentForCmd(nfcapd_data_path, start_time)
            
            for company_traffic_ip_info in company_traffic_ip_info_list:
                
                use_ip_dictObj = self.getUseIpInCidr(company_traffic_ip_info[3], one_hour_netflow_file_path)
                
                dst_ip_list = use_ip_dictObj['dst_ip_list']
                
                for host_ip in use_ip_dictObj['scr_ip_list']:
                    
                    nfdump_src_aggre_cmd = self.makeCommandString(self.nfdump_src_aggre_cmd_fmt, componentDictObj['existing_files_list'], host_ip)
                    src_outputs = self.commonFuncObj.exeCmd_getOutputString([nfdump_src_aggre_cmd])
                    max_in_bps = self.maxBps(src_outputs, componentDictObj['interval_min_list'])
                    
                    if host_ip in dst_ip_list:
                        nfdump_dst_aggre_cmd = self.makeCommandString(self.nfdump_dst_aggre_cmd_fmt, componentDictObj['existing_files_list'], host_ip)
                        dst_outputs = self.commonFuncObj.exeCmd_getOutputString([nfdump_dst_aggre_cmd])
                        max_out_bps = self.maxBps(dst_outputs, componentDictObj['interval_min_list'])
                        dst_ip_list.remove(host_ip)
                    else:
                        max_out_bps = 0
                    
                    self.insertTCompanyTrafficHour(tCompanyTrafficHourDaoObj, host_ip, execute_time_str, max_in_bps, max_out_bps, company_traffic_ip_info)
                
                for host_ip in dst_ip_list:
                    
                    nfdump_dst_aggre_cmd = self.makeCommandString(self.nfdump_dst_aggre_cmd_fmt, componentDictObj['existing_files_list'], host_ip)
                    dst_outputs = self.commonFuncObj.exeCmd_getOutputString([nfdump_dst_aggre_cmd])
                    max_out_bps = self.maxBps(dst_outputs, componentDictObj['interval_min_list'])
                    
                    self.insertTCompanyTrafficHour(tCompanyTrafficHourDaoObj, host_ip, execute_time_str, 0, max_out_bps, company_traffic_ip_info)
            
                self.commonDbObj.commitDB()    
        except:
            raise
        finally:
            if connectDbObj != None:
                self.commonDbObj.disConnectDB()
                
    
    def insertTCompanyTrafficHour(self, tCompanyTrafficHourDaoObj, host_ip, dtCollectDate, max_in_bps, max_out_bps, company_traffic_ip_info):
        
        try:
            insert_data = (host_ip,
                           dtCollectDate,
                           max_in_bps,
                           max_out_bps,
                           company_traffic_ip_info[0],
                           company_traffic_ip_info[1],
                           company_traffic_ip_info[2],
                           company_traffic_ip_info[3])
            
            tCompanyTrafficHourDaoObj.insert(insert_data)
        except:
            raise
        
     
    #
    def maxBps(self, outputs, interval_min_list):
        
        maxBps = 0
        
        try:
            num = 0
            index = 0
            count = 0
            
            for output in outputs:
                
                if output == "":
                    continue
                
                if num < int(output):
                    num = int(output)
                    index = count
                
                count += 1
            
            maxBps = num * 8 * int(self.sampling_rate) / int(interval_min_list[index])
        except:
            raise
            
        return maxBps

    #
    def makeCommandString(self, nfdump_cmd_fmt, existing_files_list, ip):
        
        nfdump_cmd = ""
        
        for existing_file in existing_files_list:
            nfdump_cmd = str(nfdump_cmd) + nfdump_cmd_fmt.format(nfcapd_data_file_path=existing_file, ip=ip)
        
        nfdump_cmd = re.sub("&&", "&& ", nfdump_cmd)
        nfdump_cmd = re.sub("&& $", "", nfdump_cmd)

        return nfdump_cmd

    #
    def getUseIpInCidr(self, cidr, one_hour_netflow_file_path):
        
        use_ip_dictObj = {}
        
        try:
            src_use_ip_confirm_cmd = self.src_use_ip_confirm_cmd_fmt.format(one_hour_netflow_file_path=one_hour_netflow_file_path,
                                                                            cidr=cidr)
            
            outputs = self.commonFuncObj.exeCmd_getOutputString([src_use_ip_confirm_cmd])
            
            scr_ip_list = []
            
            for output in outputs:
                
                if re.match(".*[A-Z]", output) or output == '':
                    continue
                
                ip = re.sub("\s", "", output)
                if self.commonFuncObj.addressInNetwork(ip, cidr) is True:
                    scr_ip_list.append(ip)
            
            dst_use_ip_confirm_cmd = self.dst_use_ip_confirm_cmd_fmt.format(one_hour_netflow_file_path=one_hour_netflow_file_path,
                                                                                      cidr=cidr)
            
            outputs = self.commonFuncObj.exeCmd_getOutputString([dst_use_ip_confirm_cmd])
            
            dst_ip_list = []
            
            for output in outputs:
                
                if re.match(".*[A-Z]", output) or output == '':
                    continue
                
                ip = re.sub("\s", "", output)
                if self.commonFuncObj.addressInNetwork(ip, cidr) is True:
                    dst_ip_list.append(ip)
        
            use_ip_dictObj['scr_ip_list'] = scr_ip_list
            use_ip_dictObj['dst_ip_list'] = dst_ip_list
        except:
            raise
        
        return use_ip_dictObj
        
    
    # 
    def getComponentForCmd(self, nfcapd_data_path, start_time):
        
        componentDictObj = {}
        
        existing_files_list = []
        interval_min_list = []
        
        execute_time = start_time
        end_time = start_time + timedelta(hours=1)
        
        while True:
            if end_time == execute_time:
                break
             
            try:
                self.commonFuncObj.isExistFilePath("%s/nfcapd.%s" % (nfcapd_data_path, execute_time.strftime('%Y%m%d%H%M')))
            except:
                execute_time = execute_time + timedelta(minutes=5)
                continue
            
            nfdump_get_timestamp_cmd = self.nfdump_get_timestamp_cmd_fmt.format(nfcapd_data_file_path="%s/nfcapd.%s" % (nfcapd_data_path, execute_time.strftime('%Y%m%d%H%M')), ip="0.0.0.0")
            
            existing_files_list.append("%s/nfcapd.%s" % (nfcapd_data_path, execute_time.strftime('%Y%m%d%H%M')))
            
            try:
                outputs = self.commonFuncObj.exeCmd_getOutputString([nfdump_get_timestamp_cmd])
                
                for output in outputs:
                    if re.match("^Time window: .*", output):
                        date_info = output.replace("Time window: ", "").split(" - ")
                        interval_datetime = datetime.strptime(date_info[1], '%Y-%m-%d %H:%M:%S') - datetime.strptime(date_info[0], '%Y-%m-%d %H:%M:%S')
                        
                        interval_min_list.append(int(interval_datetime.seconds))
            except:
                interval_min_list.append(self.interval_minutes)
                
            execute_time = execute_time + timedelta(minutes=5)
        
        componentDictObj['existing_files_list'] = existing_files_list;
        componentDictObj['interval_min_list'] = interval_min_list;
        
        return componentDictObj
            
        
    # db로부터 수집하고자 하는 ip 트래픽 정보의 대역을 취득
    def getCompanyTrafficIpInfoList(self, connectDbObj):
        
        company_traffic_ip_info_list = []
        
        try:
            # customer_cidr_info 로 투어 정보 가져오기
            tCompanyTrafficIpInfoDaoObj = TCompanyTrafficIpInfoDao(connectDbObj, self.loggerObj)
            company_traffic_ip_info_list = tCompanyTrafficIpInfoDaoObj.selectForNfdump()
        except:
            raise
        
        return company_traffic_ip_info_list
