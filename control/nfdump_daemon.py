# -*- coding: utf-8 -*-
'''
Created on 2014. 11. 17.

@author: ko
'''




from datetime import timedelta, datetime
import time
import traceback

from common.commonFunc import CommonFunc
from common.daemon import Daemon
from common.logger import Logger
from control.control_common import ControlCommonFunc


class NfdumpDaemon(Daemon):

    #
    def __init__(self):
        
        config_filepath = "./conf/nfdump.conf"
        session_name = "nfdump_session"
        
        self.className = self.__class__.__name__
        
        self.loggerObj = Logger()
        self.commonFuncObj = CommonFunc(self.loggerObj)
        self.confObj = self.commonFuncObj.getConfigObj(config_filepath)
        self.controlCommonFuncObj = ControlCommonFunc(self.commonFuncObj, self.loggerObj, self.confObj)
        self.nfcapd_data_path = self.commonFuncObj.getConfValue(self.confObj, session_name, "nfcapd_data_path", "/var/nfsen/profiles-data/live/gw/{year:04d}/{month:02d}/{day:02d}")
        self.next_start_time = None
        self.err_msg = None
        
    #
    def run_forever(self):
        
        while True:
            time.sleep(10)
            
            self.main_process()
     
    #
    def main_process(self):
        
        try:
            
            now_datetime_obj = self.commonFuncObj.getDateDetailObj()
            
            start_datetime_obj = now_datetime_obj - timedelta(hours=1)
            
            if self.next_start_time == None:

                start_time_str = "%s00" % start_datetime_obj.strftime('%Y%m%d%H')
                self.next_start_time = datetime.strptime(start_time_str, '%Y%m%d%H%M')
            
            if self.next_start_time <= start_datetime_obj:
                
                if start_datetime_obj.minute == 0 and start_datetime_obj.second == 0:
                    time.sleep(30)
                    
                self.loggerObj.info("[START TIME] : %s" % str(self.commonFuncObj.getDateDetailObj()))
                
                nfcapdDataPath = self.nfcapd_data_path.format(year=self.next_start_time.year, 
                                                              month=self.next_start_time.month, 
                                                              day=self.next_start_time.day)
                
                self.commonFuncObj.isExistFilePath(nfcapdDataPath)
                
                self.controlCommonFuncObj.mainProcess(nfcapdDataPath, self.next_start_time)
                
                self.loggerObj.info("[SUCCESS] 'success of the nfdump processing' time_zone = %s" % self.next_start_time)
                
                self.loggerObj.info("[END TIME] : %s" % str(self.commonFuncObj.getDateDetailObj()))
                
                self.next_start_time = self.next_start_time + timedelta(hours=1)
            
        except Exception, e:
            self.loggerObj.error("[FAIL] 'failure of the nfdump processing' errorMsg = %s" % str(e))
            self.loggerObj.error(str(traceback.format_exc()))
            
            if str(e) != self.err_msg:
            
                try:
                    error_msg = "[FAIL] 'failure of the nfdump processing' errorMsg = %s\n" % str(e)
                    error_msg += str(traceback.format_exc())
                    self.send_mail_for_error(error_msg)
                except Exception, e:
                    self.loggerObj.error("[FAIL] 'Error email transmission failure' errorMsg = %s " % str(e))
                
                self.err_msg = str(e)
            
            return

        
    #
    def send_mail_for_error(self, error_msg):
        
        session_name = "email"
        
        from_addr = self.commonFuncObj.getConfValue(self.confObj, session_name, "from_addr", "ksm@kinx.net")
        to_addrs = self.commonFuncObj.getConfValue(self.confObj, session_name, "to_addrs", "ksm@kinx.net,jhkim@kinx.net").replace(" ", "").split(",")
        cc_addrs = self.commonFuncObj.getConfValue(self.confObj, session_name, "cc_addrs")
        if cc_addrs is "" or cc_addrs is None:
            cc_addrs = []
        else:
            cc_addrs = cc_addrs.replace(" ", "").split(",")
        
        subject = self.commonFuncObj.getConfValue(self.confObj, session_name, "subject", "netflow international traffic collection error")
        smtp_server = self.commonFuncObj.getConfValue(self.confObj, session_name, "smtp_server", "mail.kinx.net")
        smtp_port = int(self.commonFuncObj.getConfValue(self.confObj, session_name, "smtp_port", "25"))
        
        self.commonFuncObj.send_mail(smtp_server, smtp_port, from_addr, to_addrs=to_addrs, cc_addrs=cc_addrs, subject=subject, body_text=error_msg)
        
