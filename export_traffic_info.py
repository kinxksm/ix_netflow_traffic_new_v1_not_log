# -*- coding: utf-8 -*-
'''
Created on 2014. 12. 15.

@author: ko
'''

from datetime import datetime, timedelta
import re
import sys
import traceback

from openpyxl.cell import get_column_letter
from openpyxl.styles import Border, Side, Color, Style
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.fills import PatternFill
from openpyxl.styles.fonts import Font

from common.commonDb import CommonDb
from common.commonFunc import CommonFunc
from common.logger import Logger
from db.tCompanyTrafficHour import TCompanyTrafficHourDao
from db.tCompanyTrafficIpInfo import TCompanyTrafficIpInfoDao
from netaddr import IPNetwork


class ExportTrafficInfo(object):
    
    #
    def __init__(self):
        
        self.className = self.__class__.__name__
        
        session_name = "nfdump_session"
        config_filepath = "./conf/nfdump.conf"
        
        self.loggerObj = Logger()
        self.commonFuncObj = CommonFunc(self.loggerObj)
        
        confObj = self.commonFuncObj.getConfigObj(config_filepath)
        self.commonDbObj = CommonDb(confObj, self.loggerObj)
        
        self.export_dir = self.commonFuncObj.getConfValue(confObj, session_name, "export_dir", ".")
        
        self.start_datetime = None
        self.end_datetime = None
        self.diff_time = None
        
        self.style1 = Style(font=Font(name='Calibri', sz=11.0, b=False, i=False, u=None, strike=False, vertAlign=None, charset=None, outline=False, shadow=False, condense=False, extend=False, family=2.0), 
                            fill=PatternFill(patternType='solid', fgColor=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002710B70>', theme=8, tint=0.7999816888943144, type='theme'), 
                                             bgColor=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002710B70>', indexed=64, type='indexed')), 
                            border=Border(left=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002710B70>', auto=True, type='auto')), 
                                          right=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002710B70>', auto=True, type='auto')), 
                                          top=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002710B70>', auto=True, type='auto')), 
                                          bottom=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002710B70>', auto=True, type='auto')), 
                                          diagonal=Side(style=None, color=None), diagonal_direction=None, vertical=None, horizontal=None), 
                            alignment=Alignment(horizontal='general', vertical='bottom', textRotation=0, wrapText=False, shrinkToFit=False, indent=0.0, relativeIndent=0.0, justifyLastLine=False, readingOrder=0.0), number_format='General')
        
        self.style2 = Style(font=Font(name='Calibri', sz=11.0, b=False, i=False, u=None, strike=False, vertAlign=None, charset=None, outline=False, shadow=False, condense=False, extend=False, family=2.0), 
                            fill=PatternFill(patternType='solid', fgColor=Color(rgb='<openpyxl.styles.colors.RGB object at 0x00000000026D0B70>', theme=6, tint=0.7999816888943144, type='theme'), 
                                             bgColor=Color(rgb='<openpyxl.styles.colors.RGB object at 0x00000000026D0B70>', indexed=64, type='indexed')), 
                            border=Border(left=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x00000000026D0B70>', auto=True, type='auto')), 
                                          right=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x00000000026D0B70>', auto=True, type='auto')), 
                                          top=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x00000000026D0B70>', auto=True, type='auto')), 
                                          bottom=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x00000000026D0B70>', auto=True, type='auto')), 
                                          diagonal=Side(style=None, color=None), diagonal_direction=None, vertical=None, horizontal=None), 
                            alignment=Alignment(horizontal='general', vertical='bottom', textRotation=0, wrapText=False, shrinkToFit=False, indent=0.0, relativeIndent=0.0, justifyLastLine=False, readingOrder=0.0), number_format='General')
        
        
        self.style3 = Style(font=Font(name=u'\ub9d1\uc740 \uace0\ub515', sz=11.0, b=True, i=False, u=None, strike=False, 
                                            color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002860B70>', theme=1, type='theme'), vertAlign=None, charset=129, outline=False, shadow=False, condense=False, extend=False, family=3.0), 
                                  fill=PatternFill(patternType=None, fgColor=Color(rgb='00000000', tint=0.0, type='rgb'), bgColor=Color(rgb='00000000', tint=0.0, type='rgb')), 
                                  border=Border(left=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002860B70>', auto=True, type='auto')), 
                                                right=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002860B70>', auto=True, type='auto')), 
                                                top=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002860B70>', auto=True, type='auto')), 
                                                bottom=Side(style='thin', color=Color(rgb='<openpyxl.styles.colors.RGB object at 0x0000000002860B70>', auto=True, type='auto')), 
                                                diagonal=Side(style=None, color=None), diagonal_direction=None, vertical=None, horizontal=None), 
                                  alignment=Alignment(horizontal='general', vertical='bottom', textRotation=0, wrapText=False, shrinkToFit=False, indent=0.0, relativeIndent=0.0, justifyLastLine=False, readingOrder=0.0), number_format='General')
        
    #  
    def mainProcess(self, params):
        
        param_dict_obj = {}
        
        try:
            # 파라미터의 유효성을 체크
            param_dict_obj = self.checkParamValidation(params)
        except Exception, e:
            print "ERROR: %s" % str(e)
            print "USAGE: python export_traffic_info.py -nCompanySeq 'nCompanySeq' [-nContractSeq 'nContractSeq'] [-sCidr 'sCidr'] -start_date 'start_date' -end_date 'end_date'"
            print "      (*) format : YYYYmmdd"
            print "  example1 : python export_traffic_info.py -nCompanySeq 100 -nContractSeq 200 -sCidr \"192.168.0.0.24\" -start_date \"20141101\" -end_date \"20141131\""
            print "  example2 : python export_traffic_info.py -nCompanySeq 100 -start_date \"20141101\" -end_date \"20141131\""
            print "  example3 : python export_traffic_info.py -nCompanySeq 100 -nContractSeq 200 -start_date \"20141101\" -end_date \"20141131\""
            print "  example4 : python export_traffic_info.py -nCompanySeq 100 -sCidr \"192.168.0.0.24\" -start_date \"20141101\" -end_date \"20141131\""
            return 1
        
        connectDbObj = None
        
        try:
            # export data 출력 기간의 정보를 클래스 변수로 설정
            self.setDateVar(param_dict_obj)
            
            # DB이 Connect 취득
            connectDbObj = self.commonDbObj.connectDB()
            
            # DB로부터 타켓 정보 (회사명/cidr 리스트) 를 취득.
            target_dict_obj = self.getTarget(connectDbObj, param_dict_obj)
        
            company_name = target_dict_obj['sNickName']
            
            export_file_path = "%s/%s_%s_%s.xlsx" % (self.export_dir, company_name, param_dict_obj['start_date'], param_dict_obj['end_date'])
            
            tCompanyTrafficHourDao = TCompanyTrafficHourDao(connectDbObj, self.loggerObj)
                
            self.writeExportDataInfo(export_file_path, company_name, param_dict_obj['start_date'], 
                                    param_dict_obj['end_date'], target_dict_obj['cidr_list'])
            
            for target_cidr in target_dict_obj['cidr_list']:
                
                # excel 핸들링 object (book, sheet) 를 취득.
                excel_dict_obj = self.commonFuncObj.getExcelObj(export_file_path, re.sub("/", "_", target_cidr))
                
                sheet = excel_dict_obj['sheet']
                book = excel_dict_obj['book']
                
                # DB로부터 export 타켓 ip 리스트 정보를 취득 (by cidr)
                target_ip_list = self.getTargetIpListByCidr(tCompanyTrafficHourDao, target_cidr)
                
                collected_ip_list = []
                expected_ip_list = []
                
                if target_ip_list.__len__() != 0:
                    
                    ip_list_dict_obj = self.getCollectedIpList(tCompanyTrafficHourDao, target_cidr, target_ip_list)
                    collected_ip_list = ip_list_dict_obj['collected_ip_list']
                    expected_ip_list = ip_list_dict_obj['expected_ip_list']
                
                # export 정보 중 기본 정보를 excel sheet에 출력
                self.writeBasicInfo(sheet, 
                                    company_name, 
                                    param_dict_obj['start_date'], 
                                    param_dict_obj['end_date'], 
                                    target_cidr, 
                                    collected_ip_list, expected_ip_list)
                
                if collected_ip_list.__len__() != 0:
                    # ip의 트래픽 정보를 출력
                    self.writeTrafficInfo(sheet, target_cidr, collected_ip_list, tCompanyTrafficHourDao)
                
                # excel file를 저장.
                book.save(export_file_path)
                
        except Exception, e:
            print "ERROR: %s" % str(e)
            self.loggerObj.error("ERROR: %s" % str(e))
            self.loggerObj.error(str(traceback.format_exc()))
            return 1
        finally:
            if connectDbObj != None:
                self.commonDbObj.disConnectDB()
        
        return 0
    
    
    # export data 출력 기간의 정보를 클래스 변수로 설정 
    def setDateVar(self, param_dict_obj):
        
        self.start_datetime =  datetime.strptime("%s000000" % param_dict_obj['start_date'], '%Y%m%d%H%M%S')
        self.end_datetime = datetime.strptime("%s230000" % param_dict_obj['end_date'], '%Y%m%d%H%M%S')
        self.diff_time = self.end_datetime - self.start_datetime
    
    
    # ip의 트래픽 정보를 출력
    def writeTrafficInfo(self, sheet, target_cidr, target_ip_list, tCompanyTrafficHourDao):            
        
        column_count = 2
        
        for target_ip in target_ip_list:
            sheet.cell(row = 8, column = column_count).value = target_ip
            #sheet.merge_cells(start_row = 5, start_column = column_count, end_row = 5, end_column = column_count + 1)
            sheet.cell(row = 8, column = column_count).style = self.style2
            sheet.cell(row = 8, column = column_count + 1).style = self.style2
            
            sheet.cell(row = 9, column = column_count).value = "in"
            sheet.cell(row = 9, column = column_count).style = self.style2
            
            sheet.cell(row = 9, column = column_count + 1).value = "out"
            sheet.cell(row = 9, column = column_count + 1).style = self.style2
            
            sheet.column_dimensions[get_column_letter(column_count)].width = 7
            sheet.column_dimensions[get_column_letter(column_count + 1)].width = 7
            
            column_count += 2
            
        for i in range(0, self.diff_time.days + 1):
            
            next_date = self.start_datetime + timedelta(days=i)
            
            start_date_str = str(next_date)
            end_date_str = str(next_date + timedelta(hours = 23))
            
            row_count = i + 10
            column_count = 2
            
            for target_ip in target_ip_list:
                
                max_mbps_dict_obj = self.getMaxMbps(tCompanyTrafficHourDao, target_cidr, target_ip, start_date_str, end_date_str)

                if max_mbps_dict_obj['maxInMbps'] != 0:
                    sheet.cell(row = row_count, column = column_count).value = max_mbps_dict_obj['maxInMbps']
                
                if max_mbps_dict_obj['maxOutMbps'] != 0:
                    sheet.cell(row = row_count, column = column_count + 1).value = max_mbps_dict_obj['maxOutMbps']
                
                sheet.cell(row = row_count, column = column_count).style = self.style3
                sheet.cell(row = row_count, column = column_count + 1).style = self.style3
                    
                column_count += 2
                
    #
    def getMaxMbps(self, tCompanyTrafficHourDao, target_cidr, target_ip, start_date_str, end_date_str):
        
        
        max_mbps_dict_obj = {}
        
        maxInBps = tCompanyTrafficHourDao.selectMaxInBps((target_cidr, target_ip, start_date_str, end_date_str))
        maxOutBps = tCompanyTrafficHourDao.selectMaxOutBps((target_cidr, target_ip, start_date_str, end_date_str))
        
        maxInMbps = self.calculateMbps(maxInBps)
        maxOutMbps = self.calculateMbps(maxOutBps)
        
        max_mbps_dict_obj['maxInMbps'] = maxInMbps
        max_mbps_dict_obj['maxOutMbps'] = maxOutMbps
        
        return max_mbps_dict_obj
            
    #
    def calculateMbps(self, bps):
        
        maxMbps = 0
        
        if bps != 0:
            mbps = round(float(bps) / 1000000, 1)
            
            if mbps >= 1.0:
                maxMbps = mbps
                
        return maxMbps
    
    # export 정보 중 기본 정보를 excel sheet에 출력
    def writeBasicInfo(self, sheet, company_name, start_date, end_date, cidr, collected_ip_list, expected_ip_list):
        
        sheet.cell(row = 1, column = 1).value = "Customer : %s" % company_name
        sheet.cell(row = 2, column = 1).value = "Period : %s ~ %s" % (start_date, end_date)
        sheet.cell(row = 3, column = 1).value = "Ip cidr : %s" % cidr
        
        sheet.cell(row = 5, column = 1).value = "Collected IP : %s" % str(collected_ip_list)
        sheet.cell(row = 6, column = 1).value = "Expected IP : %s" % str(expected_ip_list)
        
        if collected_ip_list.__len__() == 0:
            return
            
        sheet.cell(row = 9, column = 1).value = "Date"
        sheet.cell(row = 9, column = 1).style = self.style1
        
        count = 10
        
        for i in range(0, self.diff_time.days + 1):
            next_datetime = self.start_datetime + timedelta(days=i)
            sheet.cell(row = count, column = 1).value = next_datetime.strftime('%Y/%m/%d')
            sheet.cell(row = count, column = 1).style = self.style1
            count += 1
        
    # 
    def getCollectedIpList(self, tCompanyTrafficHourDao, target_cidr, target_ip_list):
        
        ip_list_dict_obj = {}
        
        collected_ip_list = []
        expected_ip_list = []
        
        for target_ip in target_ip_list:
            
            maxInBps = tCompanyTrafficHourDao.selectMaxInBps((target_cidr, target_ip, str(self.start_datetime), str(self.end_datetime)))
            
            if maxInBps >= 1000000:
                collected_ip_list.append(target_ip.encode('utf-8'))
                continue
            
            maxOutBps = tCompanyTrafficHourDao.selectMaxOutBps((target_cidr, target_ip, str(self.start_datetime), str(self.end_datetime)))
            
            if maxOutBps >= 1000000:
                collected_ip_list.append(target_ip.encode('utf-8'))
                continue
            
            expected_ip_list.append(target_ip.encode('utf-8'))
        
        
        ip_list_dict_obj['collected_ip_list'] = collected_ip_list
        ip_list_dict_obj['expected_ip_list'] = expected_ip_list
        
        return ip_list_dict_obj
        
    # DB로부터 export 타켓 ip 리스트 정보를 취득 (by cidr)
    def getTargetIpListByCidr(self, tCompanyTrafficHourDao, cidr):
        
        method_name = traceback.extract_stack()[-1][2]
        self.loggerObj.start(self.className, method_name, tCompanyTrafficHourDao, cidr)
        
        target_ip_list = []
        
        try:
            target_ip_list = tCompanyTrafficHourDao.selectIpListInCidr(cidr, 
                                                                       str(self.start_datetime),
                                                                       str(self.end_datetime))
        except:
            raise
        
        self.loggerObj.finish(self.className, method_name, target_ip_list)
        
        return target_ip_list
    
    #
    def writeExportDataInfo(self, export_file_path, company_name, start_date, end_date, target_cidr):
    
        # excel 핸들링 object (book, sheet) 를 취득.
        excel_dict_obj = self.commonFuncObj.getExcelObj(export_file_path, "Export_Data_Info")
        
        sheet = excel_dict_obj['sheet']
        sheet.cell(row = 1, column = 1).value = "■ Export Data Info"
        sheet.column_dimensions[get_column_letter(1)].width = 2
        sheet.cell(row = 2, column = 2).value = "Customer :"
        sheet.cell(row = 2, column = 3).value = company_name
        sheet.cell(row = 3, column = 2).value = "Period :"
        sheet.cell(row = 3, column = 3).value = "%s ~ %s" % (start_date, end_date)
        sheet.cell(row = 4, column = 2).value = "Taget Cidr :"
        
        for i in range(0, target_cidr.__len__()):
            sheet.cell(row = i + 4, column = 3).value = str(target_cidr[i])
        
        excel_dict_obj['book'].save(export_file_path)    
        
    # DB로부터 타켓 정보 (회사명/cidr 리스트) 를 취득.    
    def getTarget(self, connectDbObj, param_dict_obj):
        
        target_dict_obj = {}
        
        try:
            tCompanyTrafficIpInfoDao = TCompanyTrafficIpInfoDao(connectDbObj, self.loggerObj)
            
            nCompanySeq = param_dict_obj['nCompanySeq']
            nContractSeq = None
            sCidr = None
            
            if param_dict_obj.has_key('nContractSeq'):
                nContractSeq = param_dict_obj['nContractSeq']
                
            if param_dict_obj.has_key('sCidr'):
                sCidr = param_dict_obj['sCidr']
                
            target_dict_obj = tCompanyTrafficIpInfoDao.selectWhere(nCompanySeq, nContractSeq, sCidr)
            
            if target_dict_obj.__len__() == 0:
                raise Exception("There is no information of target in DB.")
        except:
            raise
        
        return target_dict_obj
        
        
    # 파라미터의 유효성을 체크
    def checkParamValidation(self, params):
        
        # export_traffic_info.py -nCompanySeq "" -nContractSeq "" -sCidr "" -start_date "" -end_date ""
        
        param_dict_obj = {}
        
        if params.__len__() < 7 or params.__len__() > 11:
            raise Exception("Command input failure")
        
        start_date_str = None
        end_date_str = None
        
        necessary_check = 0
        
        for i in range(1, params.__len__()):
            
            if i % 2 == 1:
                if params[i] == '-nCompanySeq' or params[i] == '-nContractSeq':
                    try:
                        if params[i] == '-nCompanySeq':
                            param_dict_obj['nCompanySeq'] = int(params[i+1])
                            necessary_check += 1
                        else:
                            param_dict_obj['nContractSeq'] = int(params[i+1])
                    except Exception, e:
                        if re.match("invalid literal for int().*",str(e)):
                            raise Exception("Command input (%s) failure" % params[i])
                        
                    continue
                
                if params[i] == '-start_date' or params[i] == '-end_date':  
                    if not re.match("^(\d{4})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[01])$", params[i+1]):
                        raise Exception("Command input (%s) failure" % params[i])
                    
                    if params[i] == '-start_date':
                        start_date_str = params[i+1]
                        param_dict_obj['start_date'] = params[i+1]
                        necessary_check += 1
                    else:
                        end_date_str = params[i+1]
                        param_dict_obj['end_date'] = params[i+1]
                        necessary_check += 1
                    
                    continue
                
                try:
                    if params[i] == '-sCidr':
                        if re.match(".*/.*", params[i+1]):
                            IPNetwork(params[i+1])
                            param_dict_obj['sCidr'] = params[i+1]
                            continue
                        else:
                            raise
                except:
                    raise Exception("Command input (%s) failure" % params[i])
                
                raise Exception("Command input failure")
        
        if necessary_check != 3:
            raise Exception("Command input failure")
        
        start_date = datetime.strptime(start_date_str, '%Y%m%d')
        end_date = datetime.strptime(end_date_str, '%Y%m%d')
        
        if start_date > end_date:
            raise Exception("Command input failure (start_date>end_date)")
        
        return param_dict_obj

if __name__ == '__main__':
    
    params = sys.argv
    
    return_code = ExportTrafficInfo().mainProcess(params)
    
    sys.exit(return_code)