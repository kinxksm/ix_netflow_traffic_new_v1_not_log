# -*- coding: utf-8 -*-
'''
Created on 2014. 11. 7.

@author: ko
'''

from datetime import datetime, timedelta
import re
import sys
import traceback

from common.commonDb import CommonDb
from common.commonFunc import CommonFunc
from common.logger import Logger
from db.tCompanyTrafficHour import TCompanyTrafficHourDao


class DeleteTrafficData(object):

    #
    def __init__(self):
        
        self.className = self.__class__.__name__
        config_filepath = "./conf/nfdump.conf"
        
        self.loggerObj = Logger()
        self.commonFuncObj = CommonFunc(self.loggerObj)
        confObj = self.commonFuncObj.getConfigObj(config_filepath)
        
        self.commonDbObj = CommonDb(confObj, self.loggerObj)
        
    #
    def main_process(self, params):
        
        try:
            self.checkVaildationParam(params)
        except Exception, e:
            print "USAGE: python delete_traffic_data.py [-M]/[-D] [Month/Days]"
            print "  example1 : python delete_traffic_data.py -M 3 <-(delete the data three months ago)"
            print "  example2 : python delete_traffic_data.py -D 7 <-(delete the data seven days ago)"
            return 1

        option = params[1].upper()
            
        try:
            b_date = self.commonFuncObj.getBeforeDateTime(option, int(params[2]))
            
            self.deleteTrafficData(b_date)
        except Exception, e:
            print "ERROR: %s" % str(e)
            print str(traceback.format_exc())
            return 1
        
        return 0
        
    #
    def checkVaildationParam(self, params):
        
        if params.__len__() != 3:
            raise Exception("Command input failure") 
        
        option = params[1].upper()
        
        if option == "-M" or option == "-D":
            self
        else:
            raise Exception("Command input failure")    
         
        if re.match("^[0-9]+$", params[2]):
            self
        else:
            raise Exception("Command input failure")
        
     
    #
    def getDeleteDateTime(self, option, num):
        
        if option == "-M":       
            b_month = 0
            now = datetime.now()
            count = 0
            
            if num >= now.month:
                b_month = now.month
        
                while True:
                    b_month = 12 + b_month
                    count = count + 1
                    
                    if num < b_month:
                        break
                    
                b_month = b_month - num
            else:
                b_month = now.month - num
                
            b_date = now
                
            if count != 0:
                b_date = now.replace(year=now.year-count)
            
            b_date = b_date.replace(month=b_month)
        else:
            b_date = datetime.now() - timedelta(days = num)
            
        return b_date
    
    #
    def deleteTrafficData(self, b_date):
        
        try:
            connectDbObj  = self.commonDbObj.connectDB()
            tCompanyTrafficHourDaoObj = TCompanyTrafficHourDao(connectDbObj, self.loggerObj)
            tCompanyTrafficHourDaoObj.deletePreviousAppointedDate(b_date.strftime('%Y%m%d%H%M'))
            self.commonDbObj.commitDB()
            self.commonDbObj.disConnectDB()
        except:
            raise
        
     
if __name__ == '__main__':
    
    params = sys.argv
    
    return_code = DeleteTrafficData().main_process(params)
    
    sys.exit(return_code)
        
        