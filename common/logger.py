# -*- coding: utf-8 -*-
'''
Created on 2014. 6. 25.

@author: ko
'''

import logging.config
import traceback

class Logger():

    loggerObj = None
    
    startMsgFormat = "[START] Func : %s#%s, Param_info : [%s]"
    endMsgFormat = "[END] Func : %s#%s, Return_info : [%s]"

    def __init__(self):
        logging.config.fileConfig('./conf/log.ini')
        self.loggerObj = logging.getLogger()
    
    def getLogger(self):
        
        print self.__class__.__name__
        print traceback.extract_stack()[-1][2]
        
        return self.loggerObj
    
    #
    def start(self, class_name, method_name, *args):
        
        param_list = []
        
        for i in range(0, len(args)):
            param_list.append("param%d=%s, " % (i + 1, args[i]))
        
        if len(args) != 0:
            param_list.append("finish")
            
        argsInfo = ''.join(param_list).replace(", finish", "")
        
        self.loggerObj.debug(self.startMsgFormat % (class_name, method_name, argsInfo))
    
    #        
    def finish(self, class_name, method_name, arg=None):
        self.loggerObj.debug(self.endMsgFormat % (class_name, method_name, arg))
    
    #
    def error(self, msg):
        self.loggerObj.error(msg)
    
    #
    def warn(self, msg):
        self.loggerObj.warn(msg)
    
    #   
    def info(self, msg):
        self.loggerObj.info(msg)
    
    #
    def debug(self, msg):
        self.loggerObj.debug(msg)
        
