# -*- coding: utf-8 -*-
import MySQLdb

from common.commonFunc import CommonFunc

class CommonDb():
    
    #
    def __init__(self, confObj, loggerObj):
        
        session_name = "db"
        self.connectDbObj = None
        
        commonFuncObj = CommonFunc(loggerObj)

        try:
            
            if commonFuncObj.isExistConfSession(confObj, session_name):
                commonFuncObj.isExistConfKey(confObj, session_name, "host")
                commonFuncObj.isExistConfKey(confObj, session_name, "db")
                commonFuncObj.isExistConfKey(confObj, session_name, "user")
                commonFuncObj.isExistConfKey(confObj, session_name, "password")
            else:
                raise Exception("DB session information does not exist in config file")
        except:
            raise
        
        self.host = confObj.get("db", "host")
        self.db = confObj.get("db", "db")
        self.user = confObj.get("db", "user")
        self.passwd = confObj.get("db", "password")
        
        if confObj.has_option("db", "charset"):
            self.charset = confObj.get("db", "charset")
        else:
            self.charset = "utf8"
        
    #
    def connectDB(self):
        
        try:
            self.connectDbObj = MySQLdb.connect(host=self.host, db=self.db, user=self.user, passwd=self.passwd, charset=self.charset)
        except:
            raise
        
        return self.connectDbObj;
    
    #
    def commitDB(self):
        
        try:
            self.connectDbObj.commit()
        except:
            raise
    
    #
    def disConnectDB(self):
        
        try:
            self.connectDbObj.close()
        except:
            raise
        
