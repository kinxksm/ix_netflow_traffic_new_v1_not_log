# -*- coding: utf-8 -*-
'''
Created on 2014. 10. 23.

@author: ko
'''

from ConfigParser import ConfigParser
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text      import MIMEText
import os
import smtplib
from subprocess import Popen, PIPE 
import subprocess

from netaddr import IPNetwork, IPAddress
from openpyxl import Workbook, load_workbook

from email.Utils import formatdate


class CommonFunc():

    #
    def __init__(self, loggerObj):
        
        self.className = self.__class__.__name__
        
        self.loggerObj = loggerObj
        
    #
    def getConfigObj(self, config_filepath):
        
        if os.path.exists(config_filepath):
            pass
        else:
            raise Exception("Invalid Config File Path (%s)" % config_filepath)
            
        conf = ConfigParser()
        conf.read(config_filepath)
        
        return conf
    
    #
    def getConfValue(self, confObj, session_name, key, default_value=None):
        
        return_value = None
        
        if self.isExistConfSession(confObj, session_name) == False:
            return_value = default_value
            return default_value
        
        if confObj.has_option(session_name, key) == True:
            if confObj.get(session_name, key) == "" and default_value == None:
                return_value = None
            elif confObj.get(session_name, key) == "" and default_value != None:
                return_value = default_value
            else:
                return_value = confObj.get(session_name, key)
        elif default_value != None:
            return_value = default_value
        else:
            return_value = None
        
        return return_value   
    
    #
    def isExistConfSession(self, conf, session_name):
        return conf.has_section(session_name) 
    
    #
    def isExistConfKey(self, conf, session_name, key):
        
        if conf.has_option(session_name, key):
            pass
        else:
            raise Exception("Invalid Config Key (session_name = %s, key = %s" % (session_name, key))
    
    #    
    def isExistFilePath(self, filePath):
        
        if os.path.exists(filePath) is not True:
            
            raise Exception("The File or Directory(%s) doesn't exist" % filePath)
        
    #
    def getDateDetailObj(self):
        
        dateTimeObj = None
        
        try:
            # date Object .year
            # date Object .month
            # date Object .day
            # date Object .hour
            # date Object .minute
            # date Object .second
            
            dateTimeObj = datetime.today()
        except:
            raise
        
        return dateTimeObj
        
    #
    def exeCmd(self, cmdList):
        
        try:
            subprocess.check_call(cmdList, shell=True)
        except:
            raise
        
        
    #
    def exeCmd_getOutputString(self, cmdList):
        
        try:
            cmdResult = subprocess.check_output(cmdList, shell=True).split("\n")
        except:
            raise
        
        output_lines = []
        
        if len(cmdResult) > 0:
            for line in cmdResult:
                output_lines.append(line)
        
        return output_lines
    
    #
    def exeCmd_getStdOut(self, cmdList):
        
        p = Popen(cmdList, shell=True, stdout=PIPE)
        
        output_lines = []
        
        while 1:
            c = p.stdout.read()
            
            if not c:
                break
            
            output_lines.append(c)
        
        p.wait()
        
        return output_lines
    
            
    #
    def getHostIpRange(self, ip, subnet_bit):
        
        return_dict_obj = {}
        
        try:
            host_bit = 32 - subnet_bit
            
            ip_fmt = "%s.%s.%s.%s"  
            first_ip = None
            end_ip = None 
            
            ip_class_num = ip.split(".")
            
            divided_by = host_bit / 8
            reminder = host_bit % 8
            
            if reminder == 0:
                ip_number = 255;
            else:
                ip_number = 2 ** reminder
            
            if divided_by == 0 or (divided_by == 1 and reminder == 0):
                if reminder == 0:
                    first_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], ip_class_num[2], "0")
                    end_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], ip_class_num[2], str(ip_number))
                else:
                    if int(ip_class_num[3]) / ip_number == 0:
                        first_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], ip_class_num[2], "0")
                        end_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], ip_class_num[2], str(ip_number - 1))
                    else:
                        first_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], ip_class_num[2], str(int(ip_class_num[3]) / ip_number * ip_number))
                        end_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], ip_class_num[2], str(int(ip_class_num[3]) / ip_number * ip_number + ip_number - 1))
            
            elif divided_by == 1 or (divided_by == 2 and reminder == 0):
                if reminder == 0:
                    first_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], "0", "0")
                    end_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], "255", "255")
                else:
                    if int(ip_class_num[2]) / ip_number == 0:
                        first_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], "0", "0")
                        end_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], str(ip_number - 1), "255")
                    else:
                        first_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], str(int(ip_class_num[2]) / ip_number * ip_number), "0")
                        end_ip = ip_fmt % (ip_class_num[0], ip_class_num[1], str(int(ip_class_num[2]) / ip_number * ip_number + ip_number - 1), "255")
                        
            elif divided_by == 2 or (divided_by == 3 and reminder == 0):
                if reminder == 0:
                    first_ip = ip_fmt % (ip_class_num[0], "0", "0", "0")
                    end_ip = ip_fmt % (ip_class_num[0], "255", "255", "255")
                else:
                    if int(ip_class_num[1]) / ip_number == 0:
                        first_ip = ip_fmt % (ip_class_num[0], "0", "0", "0")
                        end_ip = ip_fmt % (ip_class_num[0], str(ip_number - 1), "255", "255")
                    else:
                        first_ip = ip_fmt % (ip_class_num[0], str(int(ip_class_num[1]) / ip_number * ip_number), "0", "0")
                        end_ip = ip_fmt % (ip_class_num[0], str(int(ip_class_num[1]) / ip_number * ip_number + ip_number - 1), "255", "255")
            
            elif divided_by == 3:
                first_ip = ip_fmt % (str(int(ip_class_num[0]) / ip_number * ip_number), "0", "0", "0")
                end_ip = ip_fmt % (str(int(ip_class_num[0]) / ip_number * ip_number + ip_number - 1), "255", "255", "255")
                
            return_dict_obj["first_ip"] = first_ip
            return_dict_obj["end_ip"] = end_ip
        except:
            raise
        
        return return_dict_obj
        
    #
    def getHostIpList(self, first_ip, end_ip):
        
        ip_list = []
        
        try:
            ip_fmt = "%s.%s.%s.%s"
            
            first_ip_class_num = first_ip.split(".")
            end_ip_class_num = end_ip.split(".")
            
            if first_ip_class_num[0] != end_ip_class_num[0]:
                for num1 in range(int(first_ip_class_num[0]), int(end_ip_class_num[0]) + 1):
                    for num2 in range(0, 256):
                        for num3 in range(0, 256):
                            for num4 in range(0, 256):
                                ip_list.append(ip_fmt % (str(num1), str(num2), str(num3), str(num4)))
            elif first_ip_class_num[1] != end_ip_class_num[1]:
                for num1 in range(int(first_ip_class_num[1]), int(end_ip_class_num[1]) + 1):
                    for num2 in range(0, 256):
                        for num3 in range(0, 256):
                            ip_list.append(ip_fmt % (first_ip_class_num[0], str(num1), str(num2), str(num3)))
            elif first_ip_class_num[2] != end_ip_class_num[2]:
                for num1 in range(int(first_ip_class_num[2]), int(end_ip_class_num[2]) + 1):
                    for num2 in range(0, 256):
                        ip_list.append(ip_fmt % (first_ip_class_num[0], first_ip_class_num[1], str(num1), str(num2)))
            else:
                for num in range(int(first_ip_class_num[3]), int(end_ip_class_num[3]) + 1):
                    ip_list.append(ip_fmt % (first_ip_class_num[0], first_ip_class_num[1], first_ip_class_num[2], str(num)))
        except:
            raise
        
        return ip_list
    
    
    def getBeforeDateTime(self, option, num):
        
        if option == "-M":       
            b_month = 0
            now = datetime.now()
            count = 0
            
            if num >= now.month:
                b_month = now.month
        
                while True:
                    b_month = 12 + b_month
                    count = count + 1
                    
                    if num < b_month:
                        break
                    
                b_month = b_month - num
            else:
                b_month = now.month - num
                
            b_date = now
                
            if count != 0:
                b_date = now.replace(year=now.year - count)
            
            b_date = b_date.replace(month=b_month)
        else:
            b_date = datetime.now() - timedelta(days=num)
        
        return b_date
    
    #
    def addressInNetwork(self, ip, net):
        
        if IPAddress(ip) in IPNetwork(net):
            return True
        else:
            return False
    
    #
    def getExcelObj(self, file_path, add_sheet_name):
        
        excel_dict_obj = {}
        
        book = None
        sheet = None
        
        if os.path.exists(file_path):
            
            book = load_workbook(file_path)
            
            if add_sheet_name in book.get_sheet_names():
                book.remove_sheet(book.get_sheet_by_name(add_sheet_name))
            
            sheet = book.create_sheet(title=add_sheet_name)
        
        else:
            
            book = Workbook()
            sheet = book.create_sheet(title=add_sheet_name)
            
        excel_dict_obj['book'] = book
        excel_dict_obj['sheet'] = sheet
        
        return excel_dict_obj
        
    #
    def send_mail(self, smtp_server, smtp_port, from_addr, to_addrs=[], cc_addrs=[], bcc_addrs=[],
                 subject="", body_html="", body_text=""):
        
        
        smtp_con = None
        
        try:
            msg = MIMEMultipart('alternative')
            msg['Subject'] = subject
            msg['From'] = from_addr
            msg['To'] = ",".join(to_addrs)
            msg["Date"]  = formatdate(localtime=True)
            
            send_addrs = to_addrs
    
            if cc_addrs != []:
                msg['Cc'] = ",".join(cc_addrs)
                send_addrs = send_addrs + cc_addrs
            if bcc_addrs != []:
                msg['Bcc'] = ",".join(bcc_addrs)
                send_addrs = send_addrs + bcc_addrs
        
            cset = 'utf-8'
        
            if body_text != "":
                attachment_text = MIMEText(body_text, 'plain', cset)
                msg.attach(attachment_text)
        
            if body_html != "":
                attachment_html = MIMEText(body_html, 'html' , cset)
                msg.attach(attachment_html)
        
            smtp_con = smtplib.SMTP(smtp_server, smtp_port)
            smtp_con.set_debuglevel(True)
            # python v3.0
            # smtp_con.send_message(msg = msg)
            smtp_con.sendmail(from_addr, send_addrs, msg.as_string())
        except:
            raise
        finally:
            if smtp_con != None:
                smtp_con.close()
                
