'''
Created on 2014. 11. 3.

@author: ko
'''


class TCompanyTrafficHourDao(object):
    
    #
    def __init__(self, connectDbObj, loggerObj):
        
        self.className = self.__class__.__name__
        self.connectDbObj = connectDbObj
        self.loggerObj = loggerObj
        
    #
    def insert(self, insert_data):
        
        # INSERT INTO ip_traffic_info (ip, collect_date, in_bytes, out_bytes, customer_id, create_date) VALUES 
        # ('192.168.0.1', STR_TO_DATE('201411031513','%Y%m%d%H%i'), 333333, 33333, 1, CURRENT_TIMESTAMP) 
        # ON DUPLICATE KEY UPDATE in_bytes = 333333, out_bytes =333333, customer_id = '2';
        insertQueryFormat = "insert into tCompanyTrafficHour (sIp, dtCollectDate, nInBps, nOutBps, nCompanySeq, nContractSeq, sNickName, sCidr, dtCreateDate) values ('{sIp}', STR_TO_DATE('{dtCollectDate}','%Y%m%d%H%i'), {nInBps}, {nOutBps}, {nCompanySeq}, {nContractSeq}, '{sNickName}', '{sCidr}', CURRENT_TIMESTAMP)  ON DUPLICATE KEY UPDATE nInBps = {nInBps}, nOutBps = {nOutBps};"
        
        try:
            insertQuery = insertQueryFormat.format(sIp = insert_data[0], 
                                               dtCollectDate = insert_data[1],
                                               nInBps = insert_data[2],
                                               nOutBps = insert_data[3],
                                               nCompanySeq = insert_data[4],
                                               nContractSeq = insert_data[5],
                                               sNickName = insert_data[6].encode('utf-8'),
                                               sCidr = insert_data[7])
        
            self.loggerObj.debug("insert Query: %s" % insertQuery)
        
        
            cursor = self.connectDbObj.cursor()
            cursor.execute(insertQuery)
        except:
            raise
        
    # 
    def deletePreviousAppointedDate(self, p_date):
        
        deleteQueryFormat = "delete from tCompanyTrafficHour where dtCollectDate < STR_TO_DATE('{date}', '%Y%m%d%H%i');"
        
        deleteQuery = deleteQueryFormat.format(date=p_date)
        
        try:
            cursor = self.connectDbObj.cursor()
            cursor.execute(deleteQuery)
        except:
            raise
        
        
    #
    def selectIpListInCidr(self, sCidr, start_date_str, end_date_str):
        
        ip_list = []
        
        try:
            selectQueryFormat = "SELECT DISTINCT(sIp) FROM tCompanyTrafficHour WHERE sCidr = '{sCidr}' AND dtCollectDate >= '{start_date}' AND dtCollectDate <= '{end_date}'"
            
            selectQuery = selectQueryFormat.format(sCidr = sCidr,
                                                   start_date = start_date_str,
                                                   end_date = end_date_str)
            
            cursor = self.connectDbObj.cursor()
            
            cursor.execute(selectQuery)
                
            rows = cursor.fetchall()
                
            for row in rows:
                ip_list.append(row[0])
        except:
            raise
            
        return ip_list
    
    #
    def selectMaxInBps(self, where_data):
        
        maxInBps = 0
        
        try:
            selectQueryForamt = "SELECT nInBps FROM tCompanyTrafficHour WHERE sCidr = '{sCidr}' AND sIp = '{sIp}' AND dtCollectDate >= '{start_date}' AND dtCollectDate <= '{end_date}' ORDER BY nInBps DESC LIMIT 1"
         
            selectQuery = selectQueryForamt.format(sCidr = where_data[0],
                                                   sIp = where_data[1],
                                                   start_date = where_data[2],
                                                   end_date = where_data[3])
            
            cursor = self.connectDbObj.cursor()
                
            cursor.execute(selectQuery)
                
            rows = cursor.fetchall()
                
            for row in rows:
                maxInBps = row[0]
        except:
            raise
            
        return maxInBps
        
        
    #
    def selectMaxOutBps(self, where_data):
        
        maxOutBps = 0
        
        try:
            selectQueryForamt = "SELECT nOutBps FROM tCompanyTrafficHour WHERE sCidr = '{sCidr}' AND sIp = '{sIp}' AND dtCollectDate >= '{start_date}' AND dtCollectDate <= '{end_date}' ORDER BY nOutBps DESC LIMIT 1"
            
            selectQuery = selectQueryForamt.format(sCidr = where_data[0],
                                                   sIp = where_data[1],
                                                   start_date = where_data[2],
                                                   end_date = where_data[3])
            
            cursor = self.connectDbObj.cursor()
                
            cursor.execute(selectQuery)
                
            rows = cursor.fetchall()
                
            for row in rows:
                maxOutBps = row[0]
            
        except:
            raise
        
        return maxOutBps
    
        
    #
    def selectForPeriod(self, ip, start_date, end_date):
        
        traffic_data_info_list = []
        
        # SELECT * FROM ip_traffic_info WHERE collectDate > '2014-11-20 00:00:00' AND collectDate < '2014-11-21 00:00:01'
        selectQueryFormat = "SELECT sIp, dtCollectDate, nInBps, nOutBps FROM tCompanyTrafficHour WHERE sIp = '{ip}' AND dtCollectDate > '{start_date}' AND dtCollectDate < '{end_date}'"
        
        selectQuery = selectQueryFormat.format(ip = ip, start_date = start_date, end_date = end_date)
        
        try:
            cursor = self.connectDbObj.cursor()
            
            cursor.execute(selectQuery)
            
            rows = cursor.fetchall()
            
            for row in rows:
                traffic_data_info = (row[0], row[1], row[2])
                traffic_data_info_list.append(traffic_data_info)
        except:
            raise
        
        return traffic_data_info_list