# -*- coding: utf-8 -*-
'''
Created on 2014. 11. 3.

@author: ko
'''

from datetime import datetime, timedelta
import re
import sys
import traceback

from common.commonFunc import CommonFunc
from common.logger import Logger
from control.control_common import ControlCommonFunc


class ControlNfdump(object):

    
    def __init__(self):
        
        config_filepath = "./conf/nfdump.conf"
        session_name = "nfdump_session"
        
        self.className = self.__class__.__name__
        
        self.loggerObj = Logger()
        self.commonFuncObj = CommonFunc(self.loggerObj)
        confObj = self.commonFuncObj.getConfigObj(config_filepath)
        self.controlCommonFuncObj = ControlCommonFunc(self.commonFuncObj, self.loggerObj, confObj)
        
        self.nfcapd_data_path = self.commonFuncObj.getConfValue(confObj, session_name, "nfcapd_data_path", "/var/nfsen/profiles-data/live/gw/{year:04d}/{month:02d}/{day:02d}")
        
    #
    def main_process(self, param):
        
        try:
            self.checkParamValidation(param)
        except Exception, e:
            print "ERROR: %s" % str(e)
            print "USAGE: python netflow_nfdump_cmd.py \"start_date(*)\" \"finish_date(*)\""
            print "      (*) format : YYYYmmddHH "
            print "  example1 : python netflow_nfdump_cmd.py 2014010100 2014010100"
            print "  example2 : python netflow_nfdump_cmd.py 2014010101 2014010203"
            return 1
        
        next_time = datetime.strptime("%s00" % param[1], '%Y%m%d%H%M') - timedelta(hours=1)
        end_time = datetime.strptime("%s00" % param[2], '%Y%m%d%H%M') - timedelta(hours=1)
        
        #cidr_list = None
        
        #if param.__len__() > 3:
        #    cidr_list = []
        #    for i in range(3, param.__len__()):
        #        cidr_list.append(param[i]) 
        
        try:
            while True:
                self.loggerObj.info("[START TIME] : %s" % str(self.commonFuncObj.getDateDetailObj()))
                
                nfcapd_data_path = self.nfcapd_data_path.format(year=next_time.year, month=next_time.month, day=next_time.day)
                self.controlCommonFuncObj.mainProcess(nfcapd_data_path, next_time)
                
                self.loggerObj.info("[SUCCESS] 'success of the nfdump processing' time_zone = %s" % str(next_time + timedelta(hours=1)))
                
                self.loggerObj.info("[END TIME] : %s" % str(self.commonFuncObj.getDateDetailObj()))
                
                if next_time == end_time:
                    break
                
                next_time = next_time + timedelta(hours=1)
       
        except Exception, e:
            print "ERROR: %s" % str(e)
            self.loggerObj.error("ERROR: %s" % str(e))
            self.loggerObj.error(str(traceback.format_exc()))
            return 1
    
        return 0
        
    #        
    def checkParamValidation(self, param):
        
        if param.__len__() != 3:
            raise Exception("The number of the argument is insufficient")
        
        for num in range(1, 3):
            if re.match("^(\d{4})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[01])(0[0-9]|1[0-9]|2[0-3])$", param[num]):
                self
            else:   
                raise Exception("date format error")
                
        start_time = datetime.strptime(param[1], '%Y%m%d%H') - timedelta(hours=1)
        end_time = datetime.strptime(param[2], '%Y%m%d%H') - timedelta(hours=1)
        
        if start_time > end_time:
            raise Exception("start_date > finish_date")

        # start_time에 해당하는 파일의 존재 여부 체크 
        nfcapd_data_path = self.nfcapd_data_path.format(year=start_time.year, month=start_time.month, day=start_time.day)
        nfcapd_file = "nfcapd.%s00" % start_time.strftime('%Y%m%d%H')
        
        try:
            self.commonFuncObj.isExistFilePath("%s/%s" % (nfcapd_data_path, nfcapd_file))
        except:
            raise Exception("The file of the date when you appointed it does not exist")
        
        # end_time에 해당하는 파일의 존재 여부 체크
        
        if start_time == end_time:
            self
        else:
            nfcapd_data_path = self.nfcapd_data_path.format(year=end_time.year, month=end_time.month, day=end_time.day)
            nfcapd_file = "nfcapd.%s55" % end_time.strftime('%Y%m%d%H')
        
        try:
            self.commonFuncObj.isExistFilePath("%s/%s" % (nfcapd_data_path, nfcapd_file))
        except:
            raise Exception("The file of the date when you appointed it does not exist")
            
        #if param.__len__() > 3:
        #    for i in range(3, param.__len__()):
        #        if re.match("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/(?:[1-9]|1[1-9]|2[1-9]|3[1-2])$", param[i]):
        #            self
        #        else:
        #            raise Exception("cidr format error")
    
if __name__ == '__main__':
        
    param = sys.argv
        
    return_code = ControlNfdump().main_process(param)
    
    sys.exit(return_code)
    