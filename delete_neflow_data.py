'''
Created on 2014. 12. 9.

@author: ko
'''

import re
import sys
import shutil

from common.commonFunc import CommonFunc
from common.logger import Logger


class DeleteNetflowData(object):

    def __init__(self):

        session_name = "nfdump_session"
        config_filepath = "./conf/nfdump.conf"
        
        self.className = self.__class__.__name__
        
        self.loggerObj = Logger()
        self.commonFuncObj = CommonFunc(self.loggerObj)
        confObj = self.commonFuncObj.getConfigObj(config_filepath)
        self.nfcapd_data_path = self.commonFuncObj.getConfValue(confObj, session_name, "nfcapd_data_path", "/var/nfsen/profiles-data/live/gw/{year:04d}/{month:02d}/{day:02d}")
        
        self.standard_year = 2014
        
    #
    def main_process(self, params):
        
        try:
            self.checkVaildationParam(params)
        except:
            print "USAGE: python delete_netflow_data.py [-M]/[-D] [Month/Days]"
            print "  example1 : python delete_netflow_data.py -M 3 <-(delete the data three months ago)"
            print "  example2 : python delete_netflow_data.py -D 7 <-(delete the data seven days ago)"
            return 1
        
        try:
            netflow_data_path = re.sub("{year.*", "", self.nfcapd_data_path)
            
            b_date = self.commonFuncObj.getBeforeDateTime(params[1].upper(), int(params[2]))
            
            for year in list(reversed(range(self.standard_year, b_date.year))):
                path = "%s/%s" % (netflow_data_path, year)
               
                try:
                    self.commonFuncObj.isExistFilePath(path)
                    shutil.rmtree(path)
                except:
                    break
                
            for month in list(reversed(range(1, b_date.month))):
                path = "%s/%s/%02d" % (netflow_data_path, b_date.year, month)
                
                try:
                    self.commonFuncObj.isExistFilePath(path)
                    shutil.rmtree(path)
                except:
                    break
                
            for day in list(reversed(range(1, b_date.day))):
                path = "%s/%s/%02d/%02d" % (netflow_data_path, b_date.year, b_date.month, day)
                
                try:
                    self.commonFuncObj.isExistFilePath(path)
                    shutil.rmtree(path)
                except:
                    break
                
        except Exception, e:
            print "ERROR: %s" % str(e)
            return 1
        
        return 0
    #
    def checkVaildationParam(self, params):
        
        if params.__len__() != 3:
            raise Exception("Command input failure") 
        
        option = params[1].upper()
        
        if option == "-M" or option == "-D":
            self
        else:
            raise Exception("Command input failure")    
         
        if re.match("^[0-9]+$", params[2]):
            self
        else:
            raise Exception("Command input failure")
        
        
if __name__ == '__main__':
    
    params = sys.argv
    return_code = DeleteNetflowData().main_process(params)
    sys.exit(return_code)